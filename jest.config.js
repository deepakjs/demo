module.exports = {
  projects: [
    '<rootDir>/apps/demo',
    '<rootDir>/libs/base',
    '<rootDir>/libs/login',
    '<rootDir>/libs/dashboard',
  ],
};
