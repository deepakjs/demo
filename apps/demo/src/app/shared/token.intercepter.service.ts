import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable, Injector } from "@angular/core";
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable()
export class TokenIntercepterService implements HttpInterceptor {

	cachedRequests: Array<HttpRequest<any>> = [];

	constructor(private router: Router) {
	}

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		// check to see if there's internet
			const token = localStorage.getItem('ACCESS_TOKEN');
			console.log(token);
			if (!token) {
				if (request.url.includes("/lite/state/save") || request.url.includes("/forgotPassword") || request.url.includes("/resetPassword") || request.url.includes("/open/connect") ||
					request.url.includes("/getConnectRegisterDetail") || request.url.includes("emailOTP") || request.url.includes("verifyOTP") ||
					request.url.includes("sendCode")) {
					console.log('why here')
					request = request.clone({
						headers:
							request.headers.set('Content-Type', 'application/json')
					});
				} else {
					console.log('comes hear')
					request = request.clone({
						headers:
							request.headers.set('Content-Type', 'application/x-www-form-urlencoded').
								set('Authorization', 'Basic dmlzaGFsOnNlY3JldA==')
					});
				}
			} else {
				console.log('why in else')
				if (request.headers.has('Content-Type')) {
					const contentType = request.headers.get('Content-Type');
                    let contentT: string | undefined
                    if(contentType) {
                        contentT = contentType;
                    } else {
                        contentT = undefined;
                    }
					request = request.clone({
						headers:
							request.headers.delete('Content-Type', contentT).
								set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN'))
					});
				} else {
					request = request.clone({
						headers:
							request.headers.set('Content-Type', 'application/json').
								set('Authorization', 'Bearer ' + token)
					});
				}
			}

			console.log(request);
			return next.handle(request).pipe(
				map((event: HttpEvent<any>) => {
					if (event instanceof HttpResponse) {
						//TODO: do stuff with response if you want
						const response = event.body;
						// this.loggerService.log("response is : ", response);
					}
					return event;
				}),
				map((event: HttpEvent<any>) => {
					if (event instanceof HttpResponse) {
						const response = event.body;

						if (response && response.data) {
							event = event.clone({ body: (response.data) });
						} else if (response && response.access_token) {
							event = event.clone({ body: (response) });
						} else {
							event = event.clone({ body: response });
						}
						return event;
					} else if (event instanceof HttpErrorResponse) {
						const response = event.error;
						throwError(response);
						return response;
					}
				}),
				//  map completed
				catchError((err: HttpErrorResponse) => {
					return throwError(err.error);
				})
			);
		}
}
