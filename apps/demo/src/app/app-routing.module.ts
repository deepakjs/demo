
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  { path: '', redirectTo: '/login', pathMatch:'full'},
  { path: 'login', loadChildren: () => import('@demo/login').then(m => m.LoginModule) },
  { path: 'dashboard', loadChildren: () => import('@demo/dashboard').then(m => m.DashboardModule) },
  { path: 'page', loadChildren: () => import('@demo/base').then(m => m.BaseModule) },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {
      // onSameUrlNavigation: 'reload',
      // anchorScrolling: 'enabled',
      // onSameUrlNavigation: 'reload',
      // scrollPositionRestoration: 'enabled',
      // preloadingStrategy: PreloadAllModules
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
