export * as DevEnvs from './environment.dev';
export * as Envs from './environment';
export * as InttEnvs from './environment.intt';
export * as PreprodEnvs from './environment.preprod';
export * as QAEnvs from './environment.qa';
export * as ProdEvn from './environment.prod';