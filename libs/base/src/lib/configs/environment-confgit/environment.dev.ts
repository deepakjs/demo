const envUrl = "https://dev-cwp.cloudoffis.com.au/COMPLIANCE";
const webURL = "https://dev-cwp.cloudoffis.com.au";

export const environment = {
    production: false,
    url: envUrl,
    profilePicUrl: envUrl,
    suffix: '/api',
    PDFTronObject: {
      webBaseURL: webURL,
      version: '5.0.0_pdftron',
    },
    tinyMceKey: 'xmi61cvgkg13gl69065j5isn7ryah61y37son290i2byj2a8'
  };