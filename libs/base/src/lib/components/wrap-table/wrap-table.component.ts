import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { GroupDescriptor, orderBy, SortDescriptor } from "@progress/kendo-data-query";
import { PageChangeEvent, RowArgs, RowStickyFn } from '@progress/kendo-angular-grid';
import { DrawerPosition } from "@progress/kendo-angular-layout";

@Component({
    selector: 'demo-wrap-table',
    templateUrl: './wrap-table.component.html',
    styleUrls: ['./wrap-table.component.scss']
})
export class WrapTableComponent implements OnInit {
  @Input() data: any; 
  @Input() element: any;
  @Input() fixedRowReverce: boolean;
  @Input() rowStickyFeature: boolean;
  public myStickyRows: number[] = [2, 4];
  expanded: boolean;
  position: DrawerPosition = "end"
  public multiple = false;
  public allowUnsort = true;
  modelclientName: string;
    
// ABN: number = 0;
  public pageSize = 8;
  public skip = 0;
  public pagedDestinations = [];
  // public loading: boolean = false;

  
  
  public dialogOpened: boolean;
  public sort: SortDescriptor[] = [
      {
        field: "clientName",
        dir: "asc",
      },
    ];
    public groups: GroupDescriptor[] = [];
    
  //   public total = this.data.length;
  constructor() {
    this.fixedRowReverce = false;
    this.expanded = false;
  }
  
  ngOnInit() {
    if(this.fixedRowReverce) {
      this.element.map(elm => (elm.locked) ? elm.headerClass="right-to-left" : elm.headerClass = 'left-to-right')
      this.element.map(elm => (elm.locked) ? elm.class="right-to-left" : elm.class = 'left-to-right')
    }
    
  }

  public rowSticky: RowStickyFn = (args: RowArgs) => {
    return this.isSticky(args.dataItem.clientId);
  }

  public isSticky(id: number): boolean {
    return this.myStickyRows.indexOf(id) >= 0;
  }
  
  public toggleStickyRow(id: number): void {
    if (this.isSticky(id)) {
      this.myStickyRows = this.myStickyRows.filter(
        (stickyId) => stickyId !== id
      );
    } else {
      this.myStickyRows.push(id);
    }
  }

  
  public sortChange(sort: SortDescriptor[]): void {
      this.sort = sort;
      this.loadProducts();
  }
    private loadProducts(): void {
      // this.gridView = {
      //   data: orderBy(this.gridData2, this.sort),
      //   total: this.gridData2.length,
      // };
    }
    public onPageChange(e: PageChangeEvent): void {
      this.skip = e.skip;
      this.pageSize = e.take;
      // this.pageData();
    }
    
    // public pageChange(event: PageChangeEvent): void {
    //   this.skip = event.skip;
    //   this.loadItems();
    // }
    // private loadItems(): void {
    //   this.data = {
    //     data: this.column.slice(this.skip, this.skip + this.pageSize),
    //     total: this.column.length,
    //   };
    // }
    // public loadMore(): void {
    //   this.loading = true;
    //   this.service.loadMore(this.pageSize).subscribe(() =>
    //     this.loading = false
    //   );
    // }

    open(rowIndex: number){
      this.dialogOpened = true;
      this.modelclientName=this.data[rowIndex].clientName;
    }
    close(){
      this.dialogOpened = false;
    }
    public groupChange(groups: GroupDescriptor[]): void {
      this.groups = groups;
      this.loadProducts();
    }
}