import { Component, HostListener, Input, OnInit } from '@angular/core';
import { DrawerPosition } from '@progress/kendo-angular-layout';

@Component({
  selector: 'demo-side-bar-wrapper',
  templateUrl: './side-bar-wrapper.component.html',
  styleUrls: ['./side-bar-wrapper.component.scss']
})
export class SideBarWrapperComponent implements OnInit {
 @Input() expandedValue: boolean;
 @Input() positionValue: DrawerPosition;
  // public expanded: boolean = true;
  // public position: DrawerPosition = "end";

  constructor() {
    this.expandedValue = false;
    this.positionValue = 'end';
   }

  ngOnInit() {
    console.log('')
  }
  @HostListener("click", ['$event'])
  clicked(event: Event) {
    // this.inside = true;
  }
  @HostListener("document:click", ['$event'])
  clickedOut(event: Event) {
    // this.some_text = this.inside
    //   ? "Event Triggered"
    //   : "Event Triggered Outside Component";
    // this.inside = false;
  }
  public openSidebar() {
    this.expandedValue = true;
  }
  public closeSidebar() {
    this.expandedValue = false;
  }

  public onExpandChange(e: any) {
    if(e === false){
      return
    }
  }
}
