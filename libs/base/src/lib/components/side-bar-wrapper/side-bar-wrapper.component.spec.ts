import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideBarWrapperComponent } from './side-bar-wrapper.component';

describe('SideBarWrapperComponent', () => {
  let component: SideBarWrapperComponent;
  let fixture: ComponentFixture<SideBarWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideBarWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideBarWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
