import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { DialogContentBase, DialogRef } from '@progress/kendo-angular-dialog';

@Component({
  selector: 'demo-common-dialog-box',
  templateUrl: './common-dialog-box.component.html',
  styleUrls: ['./common-dialog-box.component.scss']
})
export class CommonDialogBoxComponent extends DialogContentBase implements OnInit {
  @Output() public saveForm: EventEmitter<any> = new EventEmitter();
  @Input() title: string;
  @Input() contentMessage: any;
  @Input() actions: any;
  @Input() button1: string;
  @Input() themeColorbutton1: any;
  @Input() button2: string;
  @Input() themeColorbutton2: any;
  
  public colorb = 'primary';
  constructor( dialog: DialogRef) {
    super(dialog);
    this.title = '';
    this.button1 = '';
    this.button2 = ''
    this.themeColorbutton1 = 'base';
    this.themeColorbutton2 = 'base';
   }

  ngOnInit() {
    console.log('')
  }
  public onCancelAction() {
   this.dialog.close();
  }

  public onConfirmAction() {
    this.saveForm.emit(true);
  }
  
}
