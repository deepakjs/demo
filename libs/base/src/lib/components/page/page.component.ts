import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { DrawerItem, DrawerMode, DrawerPosition } from '@progress/kendo-angular-layout';
import { SortDescriptor } from '@progress/kendo-data-query';
import { DialogBoxService } from '../../service/dialog-box.service';

@Component({
  selector: 'demo-audit-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
})
export class PageComponent implements OnInit {
  public expanded: boolean;
  public position: DrawerPosition = "end";

  @Output() triggers = new EventEmitter<string>();
  fixedRowReverce: boolean;
  public column = [
    {
      name:'clientName', title:'Client Name', locked: true,width:150, show: true
    },
    {
      name:'abn', title:'ABN', sticky: false,width:150, show: true,
      celTempEnable:true,
      cellTemplate: (elm:any) => {
        return '<b >'+elm['abn'] +'</b>';
      }
    },
    {
      name:'businessCode', title:'Business Code',sticky:false,width:150, show: true
    },
    {
      name:'businessName', title:'Business Name', locked:false,width:150, show: true
    },
    {
      name:'importedFrom', title:'Imported From', sticky:true,width:150, show: true
    },
    {
      name:'lastImportedon', title:'Last Imported on',sticky:false,width:150, show: true
    },
    {
      name:'latestJob', title:'Latest Job', locked:false,width:150, show: true
    },
    {
      name:'mapedAccountant', title:'Maped Accountant',sticky:false,width:150, show: true
    },
    {
      name:'action', title:'Action',sticky:false,width:150, show: true
    }
  ]
  
  // ABN: number = 0;

  public pagedDestinations = [];
  public pageSize = 20;
  // public loading: boolean;
  public expandMode: DrawerMode = "overlay";
  
  public sort: SortDescriptor[] = [
    {
      field: "clientName",
      dir: "asc",
    },
  ];
  public items: Array<DrawerItem> = [
    { text: "Inbox", icon: "k-i-inbox", selected: true },
    { separator: true },
    { text: "Notifications", icon: "k-i-bell" },
    { text: "Calendar", icon: "k-i-calendar" },
    { separator: true },
    { text: "Attachments", icon: "k-i-hyperlink-email" },
    { text: "Favourites", icon: "k-i-star-outline" },
  ];
  public gridData2: any[] = [
    {
        clientId: 1,
        clientName: 'ATO 2 Super Fund',
        abn: '35 567 470 959',
        businessCode: 'superrecords',
        businessName: 'GetonCloud',
        importedFrom: 'class',
        lastImportedon: '29/03/2022 05:12 PM',
        latestJob: '2022',
        mapedAccountant: 'Unmapped',
    },
    {
      clientId: 2,
      clientName: 'ABC Super Fund',
      abn: '93 599 471 340',
      businessCode: 'superrecords',
      businessName: 'GetonCloud',
      importedFrom: 'class',
      lastImportedon: '29/03/2022 05:09 PM',
      latestJob: '2022',
      mapedAccountant: 'Unmapped',
    },
    {
      clientId: 3,
      clientName: 'Cloud 5 Super Fund',
      abn: '23 614 069 859',
      businessCode: 'superrecords',
      businessName: 'GetonCloud',
      importedFrom: 'class',
      lastImportedon: '21/03/2022 04:20 PM',
      latestJob: '2022',
      mapedAccountant: 'Unmapped',
    },
    {
      clientId: 4,
      clientName: 'Example 3 Superannuation Fund',
      abn: '62 601 942 698',
      businessCode: 'superrecords',
      businessName: 'GetonCloud',
      importedFrom: 'class',
      lastImportedon: '24/02/2022 11:52 PM',
      latestJob: '2020',
      mapedAccountant: 'Unmapped',
    },
    {
      clientId: 5,
      clientName: 'ATO 1 Super Fund',
      abn: '79 718 251 458',
      businessCode: 'superrecords',
      businessName: 'GetonCloud',
      importedFrom: 'class',
      lastImportedon: '24/02/2022 09:59 PM',
      latestJob: '2020',
      mapedAccountant: 'Unmapped',
    },
    {
      clientId: 6,
      clientName: 'ATO 2 Super Fund',
      abn: '35 567 470 959',
      businessCode: 'superrecords',
      businessName: 'GetonCloud',
      importedFrom: 'class',
      lastImportedon: '29/03/2022 05:12 PM',
      latestJob: '2022',
      mapedAccountant: 'Unmapped',
    },
    {
      clientId: 7,
      clientName: 'ABC Super Fund',
      abn: '93 599 471 340',
      businessCode: 'superrecords',
      businessName: 'GetonCloud',
      importedFrom: 'class',
      lastImportedon: '29/03/2022 05:09 PM',
      latestJob: '2022',
      mapedAccountant: 'Unmapped',
    },
    {
      clientId: 8,
      clientName: 'Cloud 5 Super Fund',
      abn: '23 614 069 859',
      businessCode: 'superrecords',
      businessName: 'GetonCloud',
      importedFrom: 'class',
      lastImportedon: '21/03/2022 04:20 PM',
      latestJob: '2022',
      mapedAccountant: 'Unmapped',
    },
    {
      clientId: 9,
      clientName: 'Example 3 Superannuation Fund',
      abn: '62 601 942 698',
      businessCode: 'superrecords',
      businessName: 'GetonCloud',
      importedFrom: 'class',
      lastImportedon: '24/02/2022 11:52 PM',
      latestJob: '2020',
      mapedAccountant: 'Unmapped',
    },
    {
      clientId: 10,
      clientName: 'ATO 1 Super Fund',
      abn: '79 718 251 458',
      businessCode: 'superrecords',
      businessName: 'GetonCloud',
      importedFrom: 'class',
      lastImportedon: '24/02/2022 09:59 PM',
      latestJob: '2020',
      mapedAccountant: 'Unmapped',
    },
    {
      clientId: 11,
      clientName: 'ATO 1 Super Fund',
      abn: '79 718 251 458',
      businessCode: 'superrecords',
      businessName: 'GetonCloud',
      importedFrom: 'class',
      lastImportedon: '24/02/2022 09:59 PM',
      latestJob: '2020',
      mapedAccountant: 'Unmapped',
    },
    {
      clientId: 12,
      clientName: 'ATO 1 Super Fund',
      abn: '79 718 251 458',
      businessCode: 'superrecords',
      businessName: 'GetonCloud',
      importedFrom: 'class',
      lastImportedon: '24/02/2022 09:59 PM',
      latestJob: '2020',
      mapedAccountant: 'Unmapped',
    },
    {
      clientId: 13,
      clientName: 'ATO 1 Super Fund',
      abn: '79 718 251 458',
      businessCode: 'superrecords',
      businessName: 'GetonCloud',
      importedFrom: 'class',
      lastImportedon: '24/02/2022 09:59 PM',
      latestJob: '2020',
      mapedAccountant: 'Unmapped',
    },
    {
      clientId: 14,
      clientName: 'ATO 1 Super Fund',
      abn: '79 718 251 458',
      businessCode: 'superrecords',
      businessName: 'GetonCloud',
      importedFrom: 'class',
      lastImportedon: '24/02/2022 09:59 PM',
      latestJob: '2020',
      mapedAccountant: 'Unmapped',
    },
    {
      clientId: 15,
      clientName: 'ATO 1 Super Fund',
      abn: '79 718 251 458',
      businessCode: 'superrecords',
      businessName: 'GetonCloud',
      importedFrom: 'class',
      lastImportedon: '24/02/2022 09:59 PM',
      latestJob: '2020',
      mapedAccountant: 'Unmapped',
    },
  ];
 
  public total = this.gridData2.length;



  public dialogOpened = false;
  public windowOpened = false;

    sampleCustomers = [
    {
      Id: "ALFKI",
      CompanyName: "Alfreds Futterkiste",
      ContactName: "Maria Anders",
      ContactTitle: "Sales Representative",
      City: "Berlin",
    },
    {
      Id: "ANATR",
      CompanyName: "Ana Trujillo Emparedados y helados",
      ContactName: "Ana Trujillo",
      ContactTitle: "Owner",
      City: "México D.F.",
    },
    {
      Id: "ANTON",
      CompanyName: "Antonio Moreno Taquería",
      ContactName: "Antonio Moreno",
      ContactTitle: "Owner",
      City: "México D.F.",
    },
    {
      Id: "AROUT",
      CompanyName: "Around the Horn",
      ContactName: "Thomas Hardy",
      ContactTitle: "Sales Representative",
      City: "London",
    },
    {
      Id: "BERGS",
      CompanyName: "Berglunds snabbköp",
      ContactName: "Christina Berglund",
      ContactTitle: "Order Administrator",
      City: "Luleå",
    },
    {
      Id: "BLAUS",
      CompanyName: "Blauer See Delikatessen",
      ContactName: "Hanna Moos",
      ContactTitle: "Sales Representative",
      City: "Mannheim",
    },
  ]
  constructor(private dialogBoxService: DialogBoxService, private sanitizer: DomSanitizer) {
    this.expanded = false;
    this.fixedRowReverce = false;
    console.log('hello Loader')
  }

  ngOnInit(): void {
    console.log('init')
  }
  getMsg() {
    return `Are you sure you want to continue?`;
  }
  getError() {
    return `Something went wrong?`;
  }
  

  public showConfirmation() {
    this.dialogBoxService.showConfirmation(
      'Please confirm',
      this.getMsg(),
      'Cancel',
      'success',
      'Continue',
      'primary'
      );
  }

  public warningBox() {
    this.dialogBoxService.showConfirmation(
      'Error',
      this.getError(),
      '',
      'success',
      '',
      'primary'
      )
  }
  public close(component: string): void {
    if(component === 'dialog') {
      this.dialogOpened = false;
    } else {
      this.windowOpened = false;
    }
    
  }

  public open(component: string): void {
    if(component === 'dialog') {
      this.dialogOpened = true;
    } else {
      this.windowOpened = true;
    }
  }

  public action(status: string): void {
    console.log(`Dialog result: ${status}`);
    this.dialogOpened = false;
  }
  @HostListener("document:keydown", ["$event"])
  handelKeyboardEvent(event: KeyboardEvent) {
    if (event.keyCode === 27 ) {
      // this.kendoDialogExtension.close.emit();
    }
  }
  
  public openSidebar() {
    this.expanded = true;
  }
  fromChangeEvent() {
    this.triggers.emit('some Event Trigger');
  }

  trigger() {
    console.log('this is search')
  }
  // public loadMore(): void {
  //   this.loading = true;
  //   this.gridData2
  //     .loadMore(this.pageSize)
  //     .subscribe(() => (this.loading = false));
  // }
}
