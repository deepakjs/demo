import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { LoaderComponent } from './components/loader/loader.component';
import { PageComponent } from './components/page/page.component';
import { ButtonsModule } from "@progress/kendo-angular-buttons";
import { LayoutModule } from "@progress/kendo-angular-layout";
import { DateInputsModule } from "@progress/kendo-angular-dateinputs";
import { LabelModule } from "@progress/kendo-angular-label";
import { InputsModule } from "@progress/kendo-angular-inputs";
import { DialogsModule } from "@progress/kendo-angular-dialog";
// import { SideBarWrapperComponent } from './components/side-bar-wrapper/side-bar-wrapper.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DialogBoxService } from './service/dialog-box.service';
import { CommonDialogBoxComponent } from './components/common-dialog-box/common-dialog-box.component';
import { SideBarWrapperComponent } from './components/side-bar-wrapper/side-bar-wrapper.component';
import { GridModule, ExcelModule, PDFModule } from '@progress/kendo-angular-grid';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { WrapTableComponent } from './components/wrap-table/wrap-table.component';
import { SanitizedHtmlPipe } from './service/sanitized-html.pipe';
import { ModuleWithProviders } from '@angular/compiler/src/core';

@NgModule({
  imports: [
    CommonModule,
    ButtonsModule,
    LayoutModule,
    FormsModule,
    ReactiveFormsModule,
    DateInputsModule,
    LabelModule,
    InputsModule,
    DialogsModule,
    GridModule,
    ExcelModule,
    PDFModule,
    DropDownsModule,
    RouterModule.forChild([
      {path: 'base', pathMatch: 'full', component: PageComponent} 
    ]),
  ],
  providers:[DialogBoxService],
  declarations: [LoaderComponent, PageComponent, CommonDialogBoxComponent, SideBarWrapperComponent, WrapTableComponent, SanitizedHtmlPipe],
  entryComponents: [CommonDialogBoxComponent, SideBarWrapperComponent],
  exports: [LoaderComponent, WrapTableComponent]
})
export class BaseModule {
  public static forRoot(environment: any): ModuleWithProviders {
    return {
        ngModule: BaseModule,
        providers: [
            {
                provide: 'env', // you can also use InjectionToken
                useValue: environment
            }
        ]
    };
  }
}
