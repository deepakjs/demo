import { Injectable } from '@angular/core';
import {
    EntityCollectionServiceBase,
    EntityCollectionServiceElementsFactory
  } from '@ngrx/data';

export class GeneralRequestModal {
  method: string | undefined;
  request?: any | undefined;
  url: string | undefined;
  withSufix: boolean | undefined;
}

export class BaseService<T> extends EntityCollectionServiceBase<T> {
    constructor(serviceElementsFactory: EntityCollectionServiceElementsFactory, public storeName: string) {
        super(storeName, serviceElementsFactory);
    }

    generateRequest(method: string, request: any, url: string, withSufix: boolean) {
      const requestData = new GeneralRequestModal();
      requestData.method = method;
      requestData.request = request;
      requestData.url = url;
      requestData.withSufix = withSufix;
      const requestDataString = JSON.stringify(requestData);
      return this.getByKey(requestDataString)
    }
}