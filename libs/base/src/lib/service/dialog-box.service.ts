import { Injectable } from '@angular/core';
import { ButtonThemeColor } from '@progress/kendo-angular-buttons';
import { DialogCloseResult, DialogRef, DialogService } from '@progress/kendo-angular-dialog';
import { CommonDialogBoxComponent } from '../components/common-dialog-box/common-dialog-box.component';

@Injectable()
export class DialogBoxService {

  constructor(public dialogService: DialogService) { }

  public showConfirmation(title: string, message:string, button1: string,themeColorbutton1:ButtonThemeColor, button2:string, themeColorbutton2: ButtonThemeColor) {
    
      
      const dialog: DialogRef = this.dialogService.open({
          content: CommonDialogBoxComponent,
          cssClass: "custom-css-class",
        })
    
      const dialogInfo = dialog.content.instance as CommonDialogBoxComponent;
      dialogInfo.title = title;
      dialogInfo.contentMessage = message;
      dialogInfo.button1 = button1;
      dialogInfo.themeColorbutton1 = themeColorbutton1;
      dialogInfo.button2 = button2;
      dialogInfo.themeColorbutton2 = themeColorbutton2;
      dialog.content.instance.saveForm.subscribe((result: any) => {
        console.log(result,'result');
        if (result instanceof DialogCloseResult) {
          dialog.close();
        }
      });
}

}
