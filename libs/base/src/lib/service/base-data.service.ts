import {Inject} from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { DefaultDataService, HttpUrlGenerator } from '@ngrx/data'
import { Observable } from 'rxjs';
import { environment } from '../configs/environment-confgit/environment';

export class GeneralRequestModal {
    method: string | undefined;
    request?: any | undefined;
    url: string | undefined;
}
export class BaseDataService<T> extends DefaultDataService<T> {
    listUrl:string = 'https://dev-cwp.cloudoffis.com.au/COMPLIANCE/'+ 'authorize';
    constructor(public http_mod: HttpClient, public httpUrlGenerator_mod: HttpUrlGenerator, public storeName: string, @Inject('env') public env?) {

        super(storeName, http_mod, httpUrlGenerator_mod);
    }

    
    getById(reqData: string): Observable<T> {
        console.log(this.env)
        const requestData = JSON.parse(reqData);
        let url = environment.url+requestData.url;
        if(requestData.withSufix) {
            url = environment.url+'/api'+requestData.url;
        }
        if(requestData.method === 'POST') {
            return this.http.post<T>(url, requestData.request);
        } else if(requestData.method === 'GET') {
            return this.http.get<T>(url);
        } else if(requestData.method === 'DETETE') {
            return this.http.delete<T>(url);
        } else if(requestData.method === 'PUT') {
            return this.http.put<T>(url, requestData.request);
        } else  {
            return this.http.get<T>(requestData.url);
        }
    }
    
}