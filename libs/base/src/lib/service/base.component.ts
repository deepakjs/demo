export abstract class BaseComponent {
    public loader = 0;
    public loaderFlag = false;

    constructor() {
        console.log('base is working')
    }

    setStateOfLoader(loadingObservables: any[]) {
        loadingObservables.map(load => {
          load.subscribe((res: boolean) => {
            console.log(res)
            this.displayLoader(res)
          });
        })
    }

    displayLoader(value: boolean) {
        if (value) {
          this.loader += 1;
        }
        if (this.loader > 0 && !value) {
          this.loader -= 1;
        }
        if (this.loader === 1) {
          this.loaderFlag = true;
        }
        if (this.loader === 0) {
            this.loaderFlag = false;
        }
    }
}