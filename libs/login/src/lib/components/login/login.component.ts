import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { map, skip } from "rxjs/operators";
import { LoginRequestModel } from "../../models/login.models";
import { LoginService } from "../../services/login.service";
import { BaseComponent } from "@demo/base-component";

@Component({
    selector   : 'demo-login',
    templateUrl: './login.component.html',
    styleUrls  : ['./login.component.scss']
})
export class LoginComponent extends BaseComponent implements OnInit{
    public email: string | null | undefined;
    public password: string | null | undefined;
    constructor(public router: Router, private loginService: LoginService) {
        super();
    }
    ngOnInit() {
        this.loginService.entities$.pipe(skip(1), map(data => data[0])).subscribe((res: any) => {
            if(res.access_token) {
                localStorage.setItem('ACCESS_TOKEN', res.access_token)
                this.goToDashboard();            
            }
        });
        this.setStateOfLoader([this.loginService.loading$])
    }
    goToDashboard() {
        this.router.navigate(['/dashboard'])
    }

    getLogin() {
        const loginModel =  new LoginRequestModel();
        loginModel.email = this.email;
        loginModel.password = this.password;
        const listUrl = '/authorize';
        const postData = this.getLoginBody(loginModel.email, loginModel.password);
        //is.loginService.generateRequest(METHOD, POSTDATA, URL, API_SUFIX);
        this.loginService.generateRequest('POST', postData, listUrl, false)
    }

    getLoginBody(username: string | null | undefined, password: string | null | undefined) {
        if(username && password) {
            return `grant_type=password&username=${encodeURIComponent(username)}&password=${encodeURIComponent(password)}`;
        }
        else {
            return null
        }
    }
}