export class Login {

}

export class LoginRequestModel {
    email: string | null | undefined;
    password: string | null | undefined;
}

export class LoginResponseModel {
    access_token?: string;
    authority?: string;
    expires_in?: number;
    hasActiveMapping?: string;
    litePracticeUniqueId?: string;
    refresh_token?: string;
    routeType?: string;
    scope?: string;
    token_type?: string;
    twoFactor?: boolean;
}