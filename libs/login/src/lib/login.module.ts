import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EntityDataService, EntityDefinitionService, EntityMetadataMap } from '@ngrx/data';

import { LoginComponent } from './components/login/login.component';
import { LoginDataService } from './services/login.data.service';
import { BaseModule } from '@demo/base';
const entityMetadata: EntityMetadataMap = {
  Login: {
  },
};

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BaseModule,
    RouterModule.forChild([
      {path: '', pathMatch: 'full', component: LoginComponent} 
    ]),
  ],
  providers: [LoginDataService],
  declarations:[LoginComponent]
  
})
export class LoginModule {

  constructor(
    eds: EntityDefinitionService,
    entityDataService: EntityDataService,
    loginDataService: LoginDataService,
  ) {
    eds.registerMetadataMap(entityMetadata);
    entityDataService.registerService('Login', loginDataService); // <-- register it
  }
}
