import { Injectable } from '@angular/core';
import { EntityCollectionServiceElementsFactory } from '@ngrx/data';
import { LoginResponseModel } from '../models/login.models';
import { BaseService } from '@demo/base-service';
@Injectable({
    providedIn: 'root',
  })
export class LoginService extends BaseService<LoginResponseModel> {
    constructor(serviceElementsFactory: EntityCollectionServiceElementsFactory) {
        super( serviceElementsFactory, 'Login');
    }
}