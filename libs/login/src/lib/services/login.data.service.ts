import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DefaultDataService, HttpUrlGenerator } from '@ngrx/data'
import { Observable } from 'rxjs';
import { Login, LoginRequestModel, LoginResponseModel } from '../models/login.models';
import { BaseDataService } from '@demo/base-data-service';
import { map } from 'rxjs/operators';

@Injectable()
export class LoginDataService extends BaseDataService<LoginResponseModel> {

    constructor(public http_moda: HttpClient, public httpUrlGenerator_moda: HttpUrlGenerator) {
        super(http_moda, httpUrlGenerator_moda, 'Login');
    }
}